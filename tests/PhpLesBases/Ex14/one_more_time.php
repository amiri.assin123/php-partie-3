<?php

const ERROR = "Wrong Format\n";
// je vérifie mon nombre d'éléments dans mon $argv
if ($argc == 2) {
    $chaine = $argv[1];

    if (preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY) != false) {
        // je casse ma chaine pour assigner une valeur à chaque partie de ma date
        $tab = preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
        // je créé un tableau pour les jours de la semaine
        $jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
        // La même chose pour les mois
        $mois = ['Janvier' => 1, 'Fevrier' => 2, 'Mars' => 3, 'Avril' => 4, 'Mai' => 5, 'Juin' => 6, 'Juillet' => 7, 'Aout' => 8, 'Septembre' => 9, 'Octobre' => 10, 'Novembre' => 11, 'Decembre' => 12];
        // je vérifie que le nom du jour de la semaine et du mois est bien écrit
        if (in_array(ucfirst(strtolower($tab[0])), $jour) && array_key_exists(ucfirst(strtolower($tab[2])), $mois)) {
            $Chiffrejour = $tab[1];
            $Chiffremois = $mois[ucfirst(strtolower($tab[2]))];
            $ChiffreAnnee = $tab[3];
            // je split l'heure du jour pour vérifier que la valeur heure minute et seconde soit correcte et lisible
            $temps = preg_split('/:/', $tab[4], -1, PREG_SPLIT_NO_EMPTY);
            // vérification de mon heure minute et seconde
            if ($temps[0] < 24 && $temps[1] < 60 && $temps[2] < 60) {
                $heure = true;
            }
            // prendre en compte les limites du temps du jour
            elseif ($temps[0] == 24 && $temps[1] == 60 && $temps[2] == 60) {
                $heure = true;
            } else {
                $heure = false;
            }
            if (checkdate($Chiffremois, $Chiffrejour, $ChiffreAnnee) && ($heure == true)) {
                // je formate ma date pour que la fonction DateTime la comprenne
                $nom = $Chiffremois . '/' . $Chiffrejour . '/' . $ChiffreAnnee . ' ' . $temps[0] . ':' . $temps[1] . ':' . $temps[2];
                // j'utilise dateTime et gettimestamp pour avoir mon résultat en secondes
                $date = new DateTime($nom, new DateTimeZone('CET'));
                $result = $date->getTimestamp();
                echo $result . "\n";
            } else {
                echo ERROR;
            }
        } else {
            echo ERROR;
        }
        // elle ne renvoie pas d'erreur parce qu'elle ne reçoit rien.
    } else {
        echo '';
    }
} else {
    echo ERROR;
}
