<?php

namespace App\POO\Ex05;

// Impléments renvoi forcément à une interface.
// Création de la class en implémentant l'interface IFighter.
class NightsWatch implements IFighter
{
    // Création de la fonction Fight qui sera exécuter dans le fichier test
    public function fight()
    {
    }

    public function recruit($nom)
    {
        // verifier si il à la méthode fight dans leurs class
        if (method_exists($nom, 'fight')) {
            echo $nom->fight();
        }
    }
}
