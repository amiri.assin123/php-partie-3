<?php

namespace App\POO\Ex05;

// Création de la fonction fight pour Sam et Jon
interface IFighter
{
    public function fight();
}
