<?php

namespace App\POO\Ex00;

use App\POO\Ex04\Jaime;
use App\Resources\Classes\Lannister\Lannister;
use App\Resources\Classes\Stark\Sansa;

class Tyrion extends Lannister
{
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";
    public const SIZE = 'Short';
    // On redéclare les paramètres de la sous classe Tyrion

    protected function announceBirth(): void
    // Permet à la fonction de ne rien retourner ou retourner du vide
    {
        // if (!$this->needBirthAnnouncement) {
        //     return;
        // }
        // On fait un return car dans la classe parent il y en avait pas
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT;
            echo self::BIRTH_ANNOUNCEMENT;
            // echo self permet d'accéder aux propriétés depuis la définition de la classe
        }
    }
    public const DRUNK = "Not even if I'm drunk !\n";
    public const OKAY = "Let's do this.\n";

    public function sleepWith($partner)
    {
        if ($partner instanceof Jaime) {
            echo self::DRUNK;
        } elseif ($partner instanceof Sansa) {
            echo self::OKAY;
        } else {
            echo self::DRUNK;
        }
    }
}
