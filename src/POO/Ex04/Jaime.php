<?php

namespace App\POO\Ex04;

use App\POO\Ex00\Tyrion;
use App\Resources\Classes\Lannister\Lannister;
use App\Resources\Classes\Stark\Sansa;

class Jaime extends Lannister
{
    public const DRUNK = "Not even if I'm drunk !\n";
    public const OKAY = "Let's do this.\n";
    public const INCEST = "With pleasure, but only in a tower in Winterfell, then.\n";

    public function sleepwith($partner)
    {
        if ($partner instanceof Tyrion) {
            echo self::DRUNK;
        } elseif ($partner instanceof Sansa) {
            echo self::OKAY;
        } else {
            echo self::INCEST;
        }
    }
}
